<?php

include("../swcra_report_config.php");

if (empty($_GET)){
	echo "<HTML><BODY>
<h1>Click <a href='spk-swcra.exe'>here</a> to download <a href='spk-swcra.exe'>spk-swcra.exe</a></h1>
<p>
<img src='spk-swcra-user-doc.gif' width='720'>
</BODY></HTML>";
} else if ($_GET['action'] == 'submit') {
    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
    }

	$stmt = $conn->prepare("INSERT INTO report_data (random_key, query_1_result, query_2_result, query_3_result, query_4_result, query_5_result, query_6_result, query_7_result, query_8_result, query_9_result, query_10_result, query_11_result, query_12_result, query_13_result, query_14_result, query_15_result, query_16_result, query_17_result, query_18_result, query_19_result, query_20_result, query_21_result, query_22_result, query_23_result) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
	$stmt->bind_param('ssssssssssssssssssssssss', $_POST['random_key'], $_POST['query_1_result'], $_POST['query_2_result'], $_POST['query_3_result'], $_POST['query_4_result'], $_POST['query_5_result'], $_POST['query_6_result'], $_POST['query_7_result'], $_POST['query_8_result'], $_POST['query_9_result'], $_POST['query_10_result'], $_POST['query_11_result'], $_POST['query_12_result'], $_POST['query_13_result'], $_POST['query_14_result'], $_POST['query_15_result'], $_POST['query_16_result'], $_POST['query_17_result'], $_POST['query_18_result'], $_POST['query_19_result'], $_POST['query_20_result'], $_POST['query_21_result'], $_POST['query_22_result'], $_POST['query_23_result']);

	$stmt->execute();

	echo $conn->insert_id;

	$stmt->close();
	$conn->close();
} else if ($_GET['action'] == 'report') {
    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
}

    $k = $_GET['key'];
    $stmt = $conn->prepare("SELECT * FROM report_data WHERE random_key=?");
    $stmt->bind_param("s", $k);
    $stmt->execute();

    $result = $stmt->get_result();

    /* now you can fetch the results into an array - NICE */
    while ($row = $result->fetch_assoc()) {
?>

<style>
    table, th, td {
      border: 1px solid black;
      border-collapse: collapse;
      padding: 15px;
    }
</style>

<table>
    <tr>
        <td><b>Report ID</b></td>
        <td><?= $k ?></td>
    </tr>
    <tr>
        <td><b>List of SWPDM Vaults & Databases</b></td>
        <td><?= $row['query_1_result'] ?></td>
    </tr>
    <tr>
        <td><b>Quantity of Unique Documents</b></td>
        <td><?= $row['query_2_result'] ?></td>
    </tr>
    <tr>
        <td><b>Quantity of Unique Files</b></td>
        <td><?= $row['query_3_result'] ?></td>
    </tr>
    <tr>
        <td><b>Average Size of Parts</b></td>
        <td><?= $row['query_4_result'] ?></td>
    </tr>
    <tr>
        <td><b>Average Size of Assemblies</b></td>
        <td><?= $row['query_5_result'] ?></td>
    </tr>
    <tr>
        <td><b>Quantity of Active Users</b></td>
        <td><?= $row['query_6_result'] ?></td>
    </tr>
    <tr>
        <td><b>WAN IP Address</b></td>
        <td><?= $row['query_7_result'] ?></td>
    </tr>
    <tr>
        <td><b>Ping to database</b></td>
        <td><?= $row['query_8_result'] ?></td>
    </tr>
    <tr>
        <td><b>Packet loss to database</b></td>
        <td><?= $row['query_9_result'] ?></td>
    </tr>
    <tr>
        <td><b>SQL Version</b></td>
        <td><?= $row['query_10_result'] ?></td>
    </tr>
    <tr>
        <td><b>PDM Version</b></td>
        <td><?= $row['query_11_result'] ?></td>
    </tr>
    <tr>
        <td><b>License Server</b></td>
        <td><?= $row['query_12_result'] ?></td>
    </tr>
    <tr>
        <td><b>Localization Requirements</b></td>
        <td><?= $row['query_13_result'] ?></td>
    </tr>
    <tr>
        <td><b>Add-ins and Integrations</b></td>
        <td><?= $row['query_14_result'] ?></td>
    </tr>
    <tr>
        <td><b>Quantity of Workflows</b></td>
        <td><?= $row['query_15_result'] ?></td>
    </tr>
    <tr>
        <td><b>Quantity of Data Cards</b></td>
        <td><?= $row['query_16_result'] ?></td>
    </tr>
    <tr>
        <td><b>List of Replicated Vault Servers</b></td>
        <td><?= $row['query_17_result'] ?></td>
    </tr>
    <tr>
        <td><b>Number of Files in Cold Storage</b></td>
        <td><?= $row['query_18_result'] ?></td>
    </tr>
    <tr>
        <td><b>List of Replicated DB Servers</b></td>
        <td><?= $row['query_19_result'] ?></td>
    </tr>
    <tr>
        <td><b>Total Filesize of All Files</b></td>
        <td><?= $row['query_20_result'] ?></td>
    </tr>
    <tr>
        <td><b>List of Groups</b></td>
        <td><?= $row['query_21_result'] ?></td>
    </tr>
    <tr>
        <td><b>SMTP Configuration</b></td>
        <td><?= $row['query_22_result'] ?></td>
    </tr>
    <tr>
        <td><b>Automated Task + Task Servers</b></td>
        <td><?= $row['query_23_result'] ?></td>
    </tr>
</table>

<?php

    }
	$stmt->close();
    $conn->close();
} else {
    echo "Incorrect request response.";
}

?>