##
##
## Build date 21 June 2022
## support@spkaa.com
##
##
import tkinter as tk
from tkinter import *
import pyodbc
from datetime import datetime
import requests
import webbrowser
import sys
import subprocess
import uuid

insert_url = 'https://swcra.spkaa.com/swcra_report.php?action=submit'
report_url = 'https://swcra.spkaa.com/swcra_report.php?action=report&key='
email_service_url = 'https://swcra.spkaa.com/emailService/sendEmail'

class PrintLogger(): # create file like object
    #https://stackoverflow.com/questions/53721337/how-to-get-python-console-logs-on-my-tkinter-window-instead-of-a-cmd-window-whil
    def __init__(self, textbox): # pass reference to text widget
        self.textbox = textbox # keep ref

    def write(self, text):
        self.textbox.insert(tk.END, text) # write text to textbox
        self.textbox.see(tk.END)
        window.update()
            # could also scroll to end of textbox here to make sure always visible

    def flush(self): # needed for file like object
        pass

def list_databases():
    try:
        # Host value
        host_value = host_var.get()
        if host_value == '' or host_value is None:
            print('Please input a value for the host')
            return

        # User value
        user_value = user_var.get()
        if user_value == '' or user_value is None:
            print('Please input a value for the user')
            return

        # Pass value
        pass_value = pass_var.get()
        if pass_value == '' or pass_value is None:
            print('Please input a value for the pass')
            return

        conn = pyodbc.connect('Driver={SQL Server};'
                              'Server=%s;'
                              'UID=%s;'
                              'PWD=%s;'
                              'Trusted_Connection=no;' % (host_value, user_value, pass_value))

        cursor = conn.cursor()
        cursor.execute('SELECT name,database_id FROM sys.databases')
        database_list = []
        for c_i in cursor:
            database_list.append(c_i)
        #print(database_list)
        # Insert into listbox
        [window.nametowidget('database_listbox').insert("end", x[0]) for x in database_list]
    except Exception as e:
        error_log(e)
        error_log('Error grabbing database list:')


def send_email(x):
    email = email_var.get()
    #print(email)
    #print(x)
    payload = {"email": email,
            "random_key": x
            }
    headers = {"Authorization": "Bearer ASDLFPWIRUjhsalgklj137c68v54",
               'content-type': 'application/json'}
    response = requests.post(email_service_url, verify=False, json=payload, headers=headers)
    return "Email sent"

def run_report():
    lb = window.nametowidget('database_listbox')

    if len(lb.curselection()) > 1 or len(lb.curselection()) == 0:
        error_log('Please select exactly 1 database')
        return

    chosen_database = lb.get(lb.curselection()[0])

    # Host value
    host_value = host_var.get()
    if host_value == '' or host_value is None:
        print('Please input a value for the host')
        return

    # User value
    user_value = user_var.get()
    if user_value == '' or user_value is None:
        print('Please input a value for the user')
        return

    # Pass value
    pass_value = pass_var.get()
    if pass_value == '' or pass_value is None:
        print('Please input a value for the pass')
        return

    conn = pyodbc.connect('Driver={SQL Server};'
                          'Server=%s;'
                          'Database=%s;'
                          'UID=%s;'
                          'PWD=%s;'
                          'Trusted_Connection=no;' % (host_value, chosen_database, user_value, pass_value))

    cursor = conn.cursor()

    # List of SWPDM Vaults & Databases
    query_1_result = ''
    try:
        query_1_list = []
        cursor.execute('SELECT [VaultName],[DatabaseName] FROM [ConisioMasterDb].[dbo].[FileVaults]')
        for i in cursor:
            query_1_list.append("%s   %s" % (i[0], i[1]))
        query_1_result = '\n'.join(query_1_list)
    except Exception as e:
        error_log(e)
        error_log('Error while grabbing query 1 result:')
    print('Query 1 result: %s' % query_1_result)

    # Quantity of Unique Documents
    query_2_result = ''
    try:
        cursor.execute('SELECT COUNT(*) FROM DOCUMENTS')
        for i in cursor:
            query_2_result = i[0]
    except Exception as e:
        error_log(e)
        error_log('Error while grabbing query 2 result:')
    print('Query 2 result: %s' % query_2_result)

    # Quantity of Unique Files
    query_3_result = ''
    try:
        cursor.execute('SELECT COUNT(*) FROM REVISIONS')
        for i in cursor:
            query_3_result = i[0]
    except Exception as e:
        error_log(e)
        error_log('Error while grabbing query 3 result:')
    print('Query 3 result: %s' % query_3_result)

    # Average Size of Parts
    query_4_result = ''
    try:
        cursor.execute(
            'Select AVG(FileSize) from Revisions AS r LEFT JOIN Documents AS d on r.DocumentID=d.DocumentID LEFT JOIN FileExtension AS e on d.ExtensionID=e.ExtensionID WHERE Extension LIKE \'%prt%\'')
        for i in cursor:
            query_4_result = i[0]
    except Exception as e:
        error_log(e)
        error_log('Error while grabbing query 4 result:')
    print('Query 4 result: %s' % query_4_result)

    # Average Size of Assemblies
    query_5_result = ''
    try:
        cursor.execute(
            'Select AVG(FileSize) from Revisions AS r LEFT JOIN Documents AS d on r.DocumentID=d.DocumentID LEFT JOIN FileExtension AS e on d.ExtensionID=e.ExtensionID WHERE Extension LIKE \'%asm%\'')
        for i in cursor:
            query_5_result = i[0]
    except Exception as e:
        error_log(e)
        error_log('Error while grabbing query 5 result:')
    print('Query 5 result: %s' % query_5_result)

    # Quantity of Active Users
    query_6_result = ''
    try:
        cursor.execute('SELECT COUNT(*) FROM USERS WHERE Enabled=1')
        for i in cursor:
            query_6_result = i[0]
    except Exception as e:
        error_log(e)
        error_log('Error while grabbing query 6 result:')
    print('Query 6 result: %s' % query_6_result)

    # WAN IP Address
    query_7_result = ''
    try:
        query_7_result = requests.get('https://checkip.amazonaws.com').text.strip()
    except Exception as e:
        error_log(e)
        error_log('Error while grabbing query 7 result:')
    print('Query 7 result: %s' % query_7_result)

    osPlatform = sys.platform
    # # TODO: Ping to database
    query_8_result = ''
    CREATE_NO_WINDOW = 0x08000000
    if osPlatform.startswith('win'):
        process = subprocess.Popen(['ping', '-n', '5', host_value], creationflags=CREATE_NO_WINDOW, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, stdin=subprocess.DEVNULL).stdout
        for line in process:
            # print(line.rstrip().decode("utf-8"))
            if "Average = " in line.rstrip().decode("utf-8"):
                # if line.find("Average = ") != -1:
                query_8_result = line.rstrip().decode("utf-8")
        query_8_result = query_8_result.split("Average = ")[1]
    else:
        process = subprocess.Popen(['ping', '-c', '5', host_value], creationflags=CREATE_NO_WINDOW, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, stdin=subprocess.DEVNULL).stdout
        for line in process:
            # print(line.rstrip().decode("utf-8"))
            if "min/avg/max/mdev" in line.rstrip().decode("utf-8"):
                # if line.find("Average = ") != -1:
                query_8_result = line.rstrip().decode("utf-8")
        query_8_result = query_8_result.split("Average = ")[4]
    print("Query 8 result: " + query_8_result)
    #
    # # TODO: Packet loss to database
    query_9_result = ''
    if osPlatform.startswith('win'):
        process = subprocess.Popen(['ping', '-n', '5', host_value], creationflags=CREATE_NO_WINDOW, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, stdin=subprocess.DEVNULL).stdout
        for line in process:
            # print(line.rstrip().decode("utf-8"))
            if "Packets: " in line.rstrip().decode("utf-8"):
                # if line.find("Average = ") != -1:
                query_9_result = line.rstrip().decode("utf-8")
        query_9_result = query_9_result.split("Lost = ")[1][:-1]
    else:
        process = subprocess.Popen(['ping', '-c', '5', host_value], creationflags=CREATE_NO_WINDOW, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, stdin=subprocess.DEVNULL).stdout
        for line in process:
            # print(line.rstrip().decode("utf-8"))
            if "packets transmitted" in line.rstrip().decode("utf-8"):
                # if line.find("Average = ") != -1:
                query_9_result = line.rstrip().decode("utf-8")
        query_9_result = query_9_result.split("received, ")[1]
        query_9_result = query_9_result.split(",")[0]
    print("Query 9 result: " + query_9_result)

    # SQL Version
    query_10_result = ''
    try:
        cursor.execute('SELECT @@VERSION')
        for i in cursor:
            query_10_result = i[0].split(' - ')[1].split(' (')[0]
    except Exception as e:
        error_log(e)
        error_log('Error while grabbing query 10 result:')
    print('Query 10 result: %s' % query_10_result)

    # PDM Version
    query_11_result = ''
    try:
        cursor.execute('SELECT Version FROM SystemInfo')
        for i in cursor:
            query_11_result = i[0]
    except Exception as e:
        error_log(e)
        error_log('Error while grabbing query 11 result:')
    print('Query 11 result: %s' % query_11_result)

    # License Server
    query_12_result = ''
    try:
        cursor.execute('SELECT * FROM [ConisioMasterDb].[dbo].[LicenseServer]')
        for i in cursor:
            query_12_result = i[0]
    except Exception as e:
        error_log(e)
        error_log('Error while grabbing query 12 result:')
    print('Query 12 result: %s' % query_12_result)

    # Localization Requirements
    query_13_result = ''
    try:
        cursor.execute('SELECT CONVERT (varchar(256), SERVERPROPERTY(\'collation\'))')
        for i in cursor:
            query_13_result = i[0]
    except Exception as e:
        error_log(e)
        error_log('Error while grabbing query 13 result:')
    print('Query 13 result: %s' % query_13_result)

    # Add-ins and Integrations
    query_14_result = ''
    try:
        cursor.execute('SELECT AddInName FROM Reactors')
        for i in cursor:
            query_14_result = i[0]
    except Exception as e:
        error_log(e)
        error_log('Error while grabbing query 14 result:')
    print('Query 14 result: %s' % query_14_result)

    # Quantity of Workflows
    query_15_result = ''
    try:
        cursor.execute('SELECT COUNT(*) FROM WORKFLOWS')
        for i in cursor:
            query_15_result = i[0]
    except Exception as e:
        error_log(e)
        error_log('Error while grabbing query 15 result:')
    print('Query 15 result: %s' % query_15_result)

    # Quantity of Data Cards
    query_16_result = ''
    try:
        cursor.execute('SELECT COUNT(*) FROM CARDS')
        for i in cursor:
            query_16_result = i[0]
    except Exception as e:
        error_log(e)
        error_log('Error while grabbing query 16 result:')
    print('Query 16 result: %s' % query_16_result)

    # List of Replicated Vault Servers
    query_17_result = ''
    try:
        cursor.execute('SELECT ArchiveServerName,VaultName FROM ARCHIVESERVERS')
        for i in cursor:
            query_17_result = i[0]
    except Exception as e:
        error_log(e)
        error_log('Error while grabbing query 17 result:')
    print('Query 17 result: %s' % query_17_result)

    # Number of Files in Cold Storage
    query_18_result = ''
    try:
        cursor.execute('SELECT COUNT(*) FROM ColdStorage')
        for i in cursor:
            query_18_result = i[0]
    except Exception as e:
        error_log(e)
        error_log('Error while grabbing query 18 result:')
    print('Query 18 result: %s' % query_18_result)

    # List of Replicated DB Servers
    query_19_result = ''
    try:
        cursor.execute('SELECT * FROM DBREPLICAS')
        for i in cursor:
            query_19_result = i[0]
    except Exception as e:
        error_log(e)
        error_log('Error while grabbing query 19 result:')
    print('Query 19 result: %s' % query_19_result)

    # Total Filesize of All Files
    query_20_result = ''
    try:
        cursor.execute('Select SUM(FileSize) from Revisions')
        for i in cursor:
            query_20_result = i[0]
    except Exception as e:
        error_log(e)
        error_log('Error while grabbing query 20 result:')
    print('Query 20 result: %s' % query_20_result)

    # List of Groups
    query_21_result = ''
    try:
        cursor.execute('Select Count (*) from Groups')
        for i in cursor:
            query_21_result = i[0]
    except Exception as e:
        error_log(e)
        error_log('Error while grabbing query 21 result:')
    print('Query 21 result: %s' % query_21_result)

    # SMTP Configuration
    query_22_result = ''
    try:
        cursor.execute('Select Settings From MailPluginSettings')
        for i in cursor:
            query_22_result = i[0]
    except Exception as e:
        error_log(e)
        error_log('Error while grabbing query 22 result:')
    print('Query 22 result: %s' % query_22_result)

    # Automated Task + Task Servers
    query_23_result = ''
    try:
        cursor.execute(
            'SELECT TaskName,HostName,Type,Version,AddInName FROM Tasks AS t LEFT JOIN TaskHosts AS th ON t.TaskID=th.TaskID LEFT JOIN Hosts AS h ON th.HostID=h.HostID LEFT JOIN Reactors AS r ON t.ReactorID=r.ReactorID')
        query_23_data = []
        for i in cursor:
            query_23_data.append('  '.join([str(x) for x in i]))
            # query_23_result = i[0]
        query_23_result = '\n'.join(query_23_data)
    except Exception as e:
        error_log(e)
        error_log('Error while grabbing query 23 result:')
    print('Query 23 result: %s' % query_23_result)

    #
    random_key = str(uuid.uuid4())
    
    # Post data
    post_resp = requests.post(insert_url, verify=False, data={
        'random_key': random_key,
        'query_1_result': query_1_result,
        'query_2_result': query_2_result,
        'query_3_result': query_3_result,
        'query_4_result': query_4_result,
        'query_5_result': query_5_result,
        'query_6_result': query_6_result,
        'query_7_result': query_7_result,
        'query_8_result': query_8_result,
        'query_9_result': query_9_result,
        'query_10_result': query_10_result,
        'query_11_result': query_11_result,
        'query_12_result': query_12_result,
        'query_13_result': query_13_result,
        'query_14_result': query_14_result,
        'query_15_result': query_15_result,
        'query_16_result': query_16_result,
        'query_17_result': query_17_result,
        'query_18_result': query_18_result,
        'query_19_result': query_19_result,
        'query_20_result': query_20_result,
        'query_21_result': query_21_result,
        'query_22_result': query_22_result,
        'query_23_result': query_23_result
    })

    insert_id = post_resp.text
    if insert_id.isnumeric():
        print("")
        print("Opening default browser to URL:")
        print(report_url + random_key)
        webbrowser.open(report_url + random_key)
        send_email(random_key)
    else:
        error_log("Something went wrong while inserting your report into the database.")

    print("")
    print("Execution Complete!")


def error_log(error):
    # TODO: Add date to front of error
    #b = window.nametowidget('error_log')
    #tb.insert("1.0", '[%s] %s\n' % (datetime.now(), error))
    print("1.0", '[%s] %s\n' % (datetime.now(), error))
    pass


# Basic Window
window = tk.Tk()
window.geometry('800x615+800+615')
window.title('SPK SWPDM Reporter')

# Field vars
host_var = StringVar()
user_var = StringVar()
pass_var = StringVar()
email_var = StringVar()

# Host Label
Label(window, text="Host:").place(x=5, y=10)

# Host field
host_field = Entry(window, textvariable=host_var).place(x=80, y=10)

# User Label
Label(window, text="User:").place(x=5, y=40)

# User field
user_field = Entry(window, textvariable=user_var).place(x=80, y=40)

# Password Label
Label(window, text="Password:").place(x=5, y=70)

# Password field
password_field = Entry(window, textvariable=pass_var).place(x=80, y=70)

# Email field
Label(window, text="Email:").place(x=5, y=100)

# User field
email_field = Entry(window, textvariable=email_var).place(x=80, y=100)

# List databases button
database_button = Button(window, width=13, height=2, text="List Databases", command=list_databases).place(x=5, y=130)

# Run report button
report_button = Button(window, width=13, height=2, text="Run Report", command=run_report).place(x=105, y=130)

# Database listbox
database_listbox = Listbox(window, width=94, height=10, name='database_listbox').place(x=220, y=10)

# Log field
log_field = Text(window, width=97, height=26, name='error_log')
log_field.place(x=5, y=185)
pl = PrintLogger(log_field)

sys.stdout = pl
#170


window.mainloop()
