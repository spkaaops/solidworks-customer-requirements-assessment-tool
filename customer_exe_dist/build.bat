REM Run as Administrator
REM Turn off Windows Real-Time Antivirus
pyinstaller -F --hidden-import=win32timezone --noconsole spk-swcra.py

ECHO.
move /Y .\dist\spk-swcra.exe ..\html\spk-swcra.exe 
ECHO.

PAUSE
rd /s /q build
rd /s /q dist
del spk-swcra.spec
