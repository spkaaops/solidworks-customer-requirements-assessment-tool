from flask import Flask, request
import smtplib, traceback
from email.mime.text import MIMEText

app = Flask(__name__)
app.bearer = 'ASDLFPWIRUjhsalgklj137c68v54'
app.secret_key = "293mOIEj39JN3M92mH821nHAp90mLQOmdo4"
from_address = "support@spkaa.com"
subject = "SPKAA Solidworks Collector Report"
body = "Hello\n\nThis is an automated message sending you your link to your report:\n" \
       "https://swcra.spkaa.com/swcra_report.php?action=report&key={randomkey}\n\nThanks,\nSPKAA"

@app.route('/sendEmail', methods=['POST'])
@app.route('/emailService/sendEmail', methods=['POST'])
def sendEmail():
    #headers = request.headers
    #bearer = headers.get('Authorization')
    #token = bearer.split()[1]
    email = request.json['email']
    random_key = request.json['random_key']

    #if token == app.bearer:
    if 1==1:
        msg = MIMEText(body.format(randomkey=random_key))
        msg['to'] = email
        msg['from'] = from_address
        msg['subject'] = subject
        msg.add_header('reply-to', "donotreply@spkaa.com")

        try:
            server_ssl = smtplib.SMTP(host="smtp-relay.gmail.com", port=587, local_hostname="spkaa.com")
            server_ssl.ehlo()
            server_ssl.starttls()
            server_ssl.sendmail(msg['from'], msg['to'], msg.as_string())
            server_ssl.close()
        except:
            print(traceback.format_exc())

    return "Email sent"

if __name__ == '__main__':
    app.run(host='0.0.0.0',port=5000)