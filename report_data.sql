-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.13-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for freelancer_edwin
CREATE DATABASE IF NOT EXISTS `spk_swcra` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `spk_swcra`;

-- Dumping structure for table freelancer_edwin.report_data
CREATE TABLE IF NOT EXISTS `report_data` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `modified_date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `random_key` text,
  `query_1_result` text,
  `query_2_result` text,
  `query_3_result` text,
  `query_4_result` text,
  `query_5_result` text,
  `query_6_result` text,
  `query_7_result` text,
  `query_8_result` text,
  `query_9_result` text,
  `query_10_result` text,
  `query_11_result` text,
  `query_12_result` text,
  `query_13_result` text,
  `query_14_result` text,
  `query_15_result` text,
  `query_16_result` text,
  `query_17_result` text,
  `query_18_result` text,
  `query_19_result` text,
  `query_20_result` text,
  `query_21_result` text,
  `query_22_result` text,
  `query_23_result` text,
  
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
